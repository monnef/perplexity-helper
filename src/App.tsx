import React, { useEffect, useRef, useState } from "react";
import { Button } from "@/components/ui/button";
import { Separator } from "@/components/ui/separator";
import { AppProvider } from "@/context/AppContext";
import { useAppContext } from "@/context/AppContext";
import { Gitlab, LinkIcon, Moon, Sun } from "lucide-react";
import "./styles/global.css";
import video from "./video/video.mkv";
import VideoPlayer from "@/components/video-player";
import {
    backup,
    fences,
    dirs,
    settings,
    tags,
    newThreadButton,
    robot,
    greasyFork,
    padOutroGif,
    robotFrame,
} from "./images";

const Feature = ({
    title,
    description,
    image,
}: {
    title: string;
    description: string;
    image: string;
}) => (
    <div className="rounded-xl border bg-card p-6 shadow-sm">
        <div className="mb-2">
            <img src={image} alt={title} className=" w-full rounded-lg" />
        </div>
        <h3 className="text-lg font-semibold">{title}</h3>
        <p className="text-sm text-muted-foreground">{description}</p>
    </div>
);

const AppContent: React.FC = () => {
    const { appData, toggleDarkMode } = useAppContext();
    const imgRef = useRef<HTMLImageElement>(null);
    const useImageDimensions = (ref: React.RefObject<HTMLImageElement>) => {
        const [dimensions, setDimensions] = useState({ width: 0, height: 0 });

        useEffect(() => {
            const updateDimensions = () => {
                if (ref.current) {
                    const { width, height } =
                        ref.current.getBoundingClientRect();
                    setDimensions({ width, height });
                }
            };

            const observer = new ResizeObserver(updateDimensions);
            if (ref.current) {
                observer.observe(ref.current);
            }

            window.addEventListener("resize", updateDimensions);
            return () => {
                observer.disconnect();
                window.removeEventListener("resize", updateDimensions);
            };
        }, [ref]);

        return dimensions;
    };

    const { width } = useImageDimensions(imgRef);

    useEffect(() => {
        document.documentElement.classList.toggle(
            "dark",
            appData.config.darkMode
        );
    }, [appData.config.darkMode]);

    const features = [
        {
            title: "Tags",
            description:
                "Create tags with labels that can quickly paste your prompts to save you the hassle of constant manual repetition.",
            image: tags,
        },
        {
            title: "Fences",
            description:
                "Organize your prompt tags into neatly recognizable fences. Choice of styles is up to you!",
            image: fences,
        },
        {
            title: "Dirs",
            description:
                "If you have too many tags and do want to give up any you might consider putting them in dirs to save space.",
            image: dirs,
        },
        {
            title: "Backup",
            description:
                "You can easily export and import your complete settings to avoid losing them.",
            image: backup,
        },
        {
            title: "Customization",
            description:
                "Tags, dirs, and groups are deeply customizable. Styles include colors, size, fonts, icons, outline type and more.",
            image: settings,
        },

        {
            title: "New Thread Button",
            description:
                "New thread button merges open new thread with your last prompt, supports auto-submit as well.",
            image: newThreadButton,
        },
    ];

    return (
        <div className="min-h-screen outer-container bg-outer-light dark:bg-outer-dark flex justify-center">
            <div className=" text-foreground inner-container inline-flex flex-col p-10 my-10 rounded-3xl">
                {/* Hero Section */}
                <section className="container mx-auto px-4 pt-4 pb-16 ">
                    <Button
                        variant="ghost"
                        size="icon"
                        onClick={toggleDarkMode}
                        className="fixed top-4 right-4 z-50"
                    >
                        {appData.config.darkMode ? (
                            <Sun className="h-5 w-5" />
                        ) : (
                            <Moon className="h-5 w-5" />
                        )}
                    </Button>
                    <div className="grid gap-8 lg:grid-cols-2 items-center">
                        <div>
                            <h1 className="text-5xl font-bold mb-4 flex flex-col space-y-2">
                                <span className="hero-title-line">Try</span>
                                <span className="hero-title-line text-perplexity-helper-primary">
                                    Perplexity helper
                                </span>
                                <span className="hero-title-line">
                                    to improve
                                </span>
                                <span className="hero-title-line">
                                    your experience
                                </span>
                            </h1>
                            <div>
                                <p className="text-lg text-muted-foreground mb-6">
                                    Perplexity helper is a{" "}
                                    <a
                                        href="https://violentmonkey.github.io/"
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        className="inline-flex items-center"
                                    >
                                        violent monkey{" "}
                                        <LinkIcon className="h-4 w-4 ml-1" />
                                    </a>{" "}
                                    script that aims to improve your Perplexity
                                    experience. Its main features are tags that
                                    can help you avoid tedious copy pasting of
                                    prompts you commonly use. You can set tags,
                                    colors, outlines and icons to help save your
                                    gpts the way you need.
                                </p>
                                <div className="flex gap-4">
                                    <VideoPlayer videoSrc={video} />
                                    <Button variant="secondary">
                                        <img
                                            src={greasyFork}
                                            alt="GreasyFork"
                                            className="w-5   h-5 mr-2"
                                        />
                                        <a
                                            href="https://greasyfork.org/en/scripts/469985-perplexity-helper"
                                            target="_blank"
                                            rel="noopener noreferrer"
                                        >
                                            GreasyFork
                                        </a>
                                    </Button>
                                </div>
                            </div>
                        </div>

                        <div className="relative flex justify-center">
                            <div
                                className="pad-outro-gif-container"
                                style={{
                                    width: `${width}px`,
                                }}
                            >
                                <img
                                    src={padOutroGif}
                                    alt="Pad Outro"
                                    className="pad-outro-gif"
                                    style={{
                                        width: `${width / 1.2}px`,
                                        top: `${width / 1.96}px`,
                                    }}
                                />
                            </div>
                            <div
                                className="absolute"
                                style={{
                                    width: `${width}px`,
                                    height: `${width}px`,
                                }}
                            >
                                <img
                                    src={robotFrame}
                                    className="max-h-[500px] w-auto absolute left-[-50px] robot-frame"
                                />
                            </div>

                            <img
                                ref={imgRef}
                                src={robot}
                                alt="Robot"
                                className="max-h-[500px] w-auto relative z-10 left-[-50px]"
                            />

                            <div
                                className="block robot-bg"
                                style={{
                                    width: `${width}px`,
                                    height: "100%",
                                    position: "absolute",
                                    top: 0,
                                }}
                            ></div>
                        </div>
                    </div>
                </section>
                <Separator wrapperClassName="dark:opacity-50 opacity-100 px-4" />

                {/* Features Grid */}
                <section className="container mx-auto px-4 py-16">
                    <h2 className="text-3xl font-bold mb-12">Features</h2>
                    <div className="grid gap-6 md:grid-cols-2 lg:grid-cols-2">
                        {features.map((feature, index) => (
                            <Feature key={index} {...feature} />
                        ))}
                    </div>
                </section>
                <Separator wrapperClassName="dark:opacity-50 opacity-100 px-4 mt-8" />
                <section className="container mx-auto px-4 py-8 flex justify-center ">
                    <div className="text-center mt-10 text-sm text-muted-foreground flex gap-4">
                        Script maintainers:{" "}
                        <a
                            href="https://gitlab.com/monnef"
                            target="_blank"
                            rel="noopener noreferrer"
                            className="flex items-center"
                        >
                            <Gitlab className="mr-1" /> monnef
                        </a>
                        <a
                            href="https://gitlab.com/Tiartyos"
                            target="_blank"
                            rel="noopener noreferrer"
                            className="flex items-center"
                        >
                            <Gitlab name="Gitlab" className="mr-1" /> Tiartyos
                        </a>
                    </div>
                </section>
            </div>
        </div>
    );
};

const App: React.FC = () => {
    return (
        <AppProvider>
            <AppContent />
        </AppProvider>
    );
};

export default App;
