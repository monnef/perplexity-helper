
export interface AppData {
    config: {
        darkMode: boolean;
        debugMode?: boolean;
    };
} 