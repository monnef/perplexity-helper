import { AppData } from '@/lib/types';

const STORAGE_KEY = 'perplexity-helper-data';

const DEFAULT_APP_DATA: AppData = {
    config: {
        darkMode: true,
        debugMode: false
    },
};

export const saveAppData = (data: AppData): void => {
    try {
        localStorage.setItem(STORAGE_KEY, JSON.stringify(data));
    } catch (error) {
        console.error('Failed to save app data:', error);
    }
};

export const loadAppData = (): AppData => {
    try {
        const storedData = localStorage.getItem(STORAGE_KEY);
        return storedData ? JSON.parse(storedData) : DEFAULT_APP_DATA;
    } catch (error) {
        console.error('Failed to load app data:', error);
        return DEFAULT_APP_DATA;
    }
};

export const validateAppData = (data: AppData): AppData => {
    // Ensure all required fields exist, use defaults if missing
    return {
        config: {
            darkMode: data.config?.darkMode ?? false,
            debugMode: data.config?.debugMode ?? false
        },
    };
};

export const resetAppData = (): AppData => {
    saveAppData(DEFAULT_APP_DATA);
    return DEFAULT_APP_DATA;
}; 