import React, { useEffect, useRef } from 'react';
import {
  Dialog,
  DialogContent,
  DialogTrigger,
} from "@/components/ui/dialog";
import { Button } from "@/components/ui/button";
import { Play } from "lucide-react";

interface VideoPlayerProps {
  videoSrc: string;

}

const VideoPlayerInner = ({ video }: { video: string }) => {
    const videoRef = useRef<HTMLVideoElement>(null);
      
        useEffect(() => {
          if (videoRef.current) {
            videoRef.current.volume = 0.1;
          }
        }, []);
      
        return (
          <video 
            ref={videoRef}
            src={video} 
            autoPlay 
            loop 
            controls 
          />
        );
      };

const VideoPlayer: React.FC<VideoPlayerProps> = ({ videoSrc }) => {
  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button>
          <Play className="mr-2" style={{ width: '1.2rem', height: '1.2rem' }} />
          View video
        </Button>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[1000px] p-10">
        <div className="aspect-video">
          <VideoPlayerInner video={videoSrc} />
        </div>
      </DialogContent>
    </Dialog>
  );
};

export default VideoPlayer; 