import React, { createContext, useContext, ReactNode } from "react";
import { useAppData } from "@/hooks/useAppData";

// Create a context type based on the return type of useAppData
type AppContextType = ReturnType<typeof useAppData>;

// Create the context with a default value
const AppContext = createContext<AppContextType | undefined>(undefined);

// Provider component
export const AppProvider: React.FC<{ children: ReactNode }> = ({
  children,
}) => {
  const appDataValue = useAppData();

  return (
    <AppContext.Provider value={appDataValue}>{children}</AppContext.Provider>
  );
};

// Custom hook to use the AppContext
export const useAppContext = () => {
  const context = useContext(AppContext);

  if (context === undefined) {
    throw new Error("useAppContext must be used within an AppProvider");
  }

  return context;
};
