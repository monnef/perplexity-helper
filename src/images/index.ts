// Import all images
import greasyFork from "./GreasyFork.svg";
import robot from "./robot.png";

//Features
import backup from "./backup.png";
import settings from "./settings.gif";
import tags from "./tags.gif";
import fences from "./fences.png";
import dirs from "./dirs.png";
import newThreadButton from "./new-thread-button.gif";
import padOutroGif from "./pad_outro.gif";
import robotFrame from "./robot_frame.png";

// Add more imports as needed

// Export all images
export { newThreadButton, backup, settings, tags, fences, dirs, greasyFork, robot, padOutroGif, robotFrame };