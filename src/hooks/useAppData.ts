import { useState, useCallback } from "react";
import { AppData } from "@/lib/types";
import {
  loadAppData,
  saveAppData,
  validateAppData,
  resetAppData,
} from "@/lib/storage";

export const useAppData = () => {
  const [appData, setAppData] = useState<AppData>(() =>
    validateAppData(loadAppData())
  );

  const updateConfig = useCallback(
    (updates: Partial<AppData["config"]>) => {
      const newData = {
        ...appData,
        config: {
          ...appData.config,
          ...updates,
        },
      };
      setAppData(newData);
      saveAppData(newData);
    },
    [appData]
  );

  const toggleDarkMode = useCallback(() => {
    updateConfig({ darkMode: !appData.config.darkMode });
  }, [appData.config.darkMode, updateConfig]);

  const resetData = useCallback(() => {
    const defaultData = resetAppData();
    setAppData(defaultData);
  }, []);

  return {
    appData,
    updateConfig,
    toggleDarkMode,
    resetData,
  };
};
