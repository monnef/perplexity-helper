/** @type {import('tailwindcss').Config} */
export default {
    darkMode: ["class"],
    content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
    theme: {
        fontFamily: {
            sans: [
                "JetBrains Mono",
                "ui-sans-serif",
                "system-ui",
                "-apple-system",
                "BlinkMacSystemFont",
                "Segoe UI",
                "Roboto",
                "Helvetica Neue",
                "Arial",
                "Noto Sans",
                "sans-serif",
            ],
            mono: [
                "JetBrains Mono",
                "ui-monospace",
                "SFMono-Regular",
                "Menlo",
                "Monaco",
                "Consolas",
                "Liberation Mono",
                "Courier New",
                "monospace",
            ],
        },
        container: {
            center: true,
            padding: "2rem",
            screens: {
                "2xl": "1400px",
            },
        },
        extend: {
            colors: {
                "perplexity-helper-primary": "#00c1ff",
                "perplexity-helper-secondary": "#2196F3",
                "background-gradient":
                    "radial-gradient(circle at top right, #170838, #04020c)",
                border: {
                    DEFAULT: "hsl(220, 13%, 65%)",
                    dark: "hsl(220 13% 35%)",
                },
                input: "hsl(var(--input))",
                ring: "hsl(var(--ring))",
                background: "hsl(var(--background))",
                foreground: "hsl(var(--foreground))",
                primary: {
                    DEFAULT: "#1079b4",
                    foreground: "white",
                },
                secondary: {
                    DEFAULT: "white",
                    foreground: "hsl(var(--secondary-foreground))",
                },
                outer: {
                    light: "white",
                    dark: "#0d0615",
                },
            },
            borderRadius: {
                lg: "var(--radius)",
                md: "calc(var(--radius) - 2px)",
                sm: "calc(var(--radius) - 4px)",
            },
            keyframes: {
                "accordion-down": {
                    from: { height: "0" },
                    to: { height: "var(--radix-accordion-content-height)" },
                },
                "accordion-up": {
                    from: { height: "var(--radix-accordion-content-height)" },
                    to: { height: "0" },
                },
            },
            animation: {
                "accordion-down": "accordion-down 0.2s ease-out",
                "accordion-up": "accordion-up 0.2s ease-out",
            },
            zIndex: {
                200: "200",
                300: "300",
                400: "400",
            },
        },
    },
    plugins: [require("tailwindcss-animate")],
};
